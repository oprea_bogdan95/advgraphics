#pragma once
#include <math.h>
#include <assert.h> 
#include "vector3.h"

typedef double Scalar;

class Versor3 {
	Versor3(Scalar _x, Scalar _y, Scalar _z);
public:

	Scalar x, y, z;
	// constructors


	static Versor3 right();
	static Versor3 left();
	static Versor3 up();
	static Versor3 down();
	static Versor3 fowrard();
	static Versor3 backward();

	// access to the coordinates: to write them
	Scalar& operator[] (int i);

	// access to the coordinates: to read them
	Scalar operator[] (int i) const;

	Vector3 operator*(Scalar k) const;

	Versor3 operator -() const;
	Versor3 operator +() const;

	bool operator ==(Versor3 vers) const;

	friend Versor3 normalize(Vector3 p);
	friend Versor3 Vector3::asVersor() const;

	Vector3 asVector() const;

	void printf() const;

};

inline Versor3 normalize(Vector3 p) {
	Scalar n = norm(p);
	return Versor3(p.x / n, p.y / n, p.z / n);
}

// cosine between a and b
inline Scalar dot(const Versor3& a, const Versor3& b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

// lenght of projection of b along a
inline Scalar dot(const Versor3& a, const Vector3& b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline Vector3 cross(const Versor3& a, const Versor3& b) {
	return Vector3(
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

inline Vector3 operator*(Scalar k, const Versor3& a) { return a * k; }

inline Versor3 nlerp(const Versor3& a, const Versor3& b, Scalar t) {
	//TODO nlerp DONE
	return normalize((1 - t) * a + t * b);
}

inline Versor3 slerp(const Versor3& a, const Versor3& b, Scalar t) {
	//TODO slerp done
	Scalar angle = acos(dot(a, b));// finds the angle
	Vector3 d1 = (sin(1 - t) * angle) / sin(angle) * a;
	Vector3 d2 = (sin(t * angle) / sin(angle)) * b;
	Vector3 summ = d1 + d2;
	return  normalize(summ);
}

// under my own resposability, I declare this vector to be unitary and therefore a VERSOR
inline Versor3 Vector3::asVersor() const {
	//TODO  assert DONE
	Vector3 myvector = Vector3(x, y, z);
	assert(norm(myvector) >= 1 + std::numeric_limits<Scalar>::epsilon() && norm(myvector) <= 1 - std::numeric_limits<Scalar>::epsilon());
	
	//Versor3 vers = normalize(myvector);
	return Versor3(x,y,z);
}


