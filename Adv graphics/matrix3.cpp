#include <iostream>
#include "vector3.h"
#include "point3.h"

#include "axis_angle.h"
#include "euler.h"
#include "quaternion.h"
#include "matrix3.h"
/* Matrix3 class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all the expected methods    */

	/* fields */
	// TODO M-Fields: which fields to store? (also add a constuctor taking these fields). DONE

Matrix3::Matrix3(Versor3 _x, Versor3 _y, Versor3 _z) : x(_x), y(_y), z(_z) {

}

// TODO M-Ide: this constructor construct the identity rotation DONE
Matrix3::Matrix3() : x(Versor3::right()), y(Versor3::up()), z(Versor3::fowrard()) {}

// constructor that takes as input the coefficient (RAW-MAJOR order!)
Matrix3::Matrix3(
	Scalar m00, Scalar m01, Scalar m02,
	Scalar m10, Scalar m11, Scalar m12,
	Scalar m20, Scalar m21, Scalar m22) : x(normalize(Vector3(m00, m10, m20))), y(normalize(Vector3(m01, m11, m21))), z(normalize(Vector3(m02, m12, m22))) {
	// TODO M-Constr DONE
}

bool Matrix3::operator == (Matrix3 mat3) {

	return	x.x == mat3.x.x && x.y == mat3.x.y && x.z == mat3.x.z &&
		y.x == mat3.y.x && y.y == mat3.y.y && y.z == mat3.y.z &&
		z.x == mat3.z.x && z.y == mat3.z.y && z.z == mat3.z.z;
}
//TODO combine two rotations (r goes first!)  DONE
Matrix3 Matrix3::operator * (Matrix3 r) {
	Matrix3 res;
	
	//1 row
	res.x.x = x.x * r.x.x + y.x * r.x.y + z.x * r.x.z;
	res.y.x = x.x * r.y.x + y.x * r.y.y + z.x * r.y.z;
	res.z.x = x.x * r.z.x + y.x * r.z.y + z.x * r.z.z;
	//2 row
	res.x.y = x.y * r.x.x + y.y * r.x.y + z.y * r.x.z;
	res.y.y = x.y * r.y.x + y.y * r.y.y + z.y * r.y.z;
	res.z.y = x.y * r.z.x + y.y * r.z.y + z.y * r.z.z;
	//3 row
	res.x.z = x.z * r.x.z + y.z * r.x.y + z.z * r.x.z;
	res.y.z = x.z * r.y.z + y.z * r.y.y + z.z * r.y.z;
	res.z.z = x.z * r.z.z + y.z * r.z.y + z.z * r.z.z;
	return res;
}

Vector3 Matrix3::apply(Vector3  v) const {
	// TODO M-App: how to apply a rotation of this type? DONE
	Vector3 result;
	result.x = v.x * x.x + v.y * y.x + v.z * z.x;
	result.y = v.x * x.y + v.y * y.y + v.z * z.y;
	result.z = v.x * x.z + v.y * y.z + v.z * z.z;

	return result;
}

// Rotations can be applied to versors or vectors just as well
Versor3 Matrix3::apply(Versor3 dir) const {
	return apply(dir.asVector()).asVersor();
}

Point3 Matrix3::apply(Point3 p) const {
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
Versor3 Matrix3::operator() (Versor3 p) { return apply(p); }
Point3  Matrix3::operator() (Point3  p) { return apply(p); }
Vector3 Matrix3::operator() (Vector3 p) { return apply(p); }

Versor3 Matrix3::axisX() const {
	return x;
}  // TODO M-Ax a DONE
Versor3 Matrix3::axisY() const {
	return y;
}  // TODO M-Ax b DONE
Versor3 Matrix3::axisZ() const {
	return z;
}  // TODO M-Ax c DONE


Matrix3 Matrix3::inverse() const {
	//TODO inverse DONE
	Matrix3 temp = *this;

	return temp.transposed();
}

void Matrix3::invert() {
	//TODO invert DONE
	transpose();
}

Matrix3 Matrix3::transposed() const {
	// TODO M-Transp a DONE
	Matrix3 temp = *this;
	temp.transpose();
	return temp;
}

void Matrix3::transpose() {
	// TODO M-Transp b DONE
	Scalar tempX_Y = x.y;
	x.y = y.x;
	y.x = tempX_Y;
	Scalar tempX_Z = x.y;
	x.z = z.x;
	z.x = tempX_Z;
	Scalar tempY_Z = x.y;
	y.z = z.y;
	z.y = tempY_Z;
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
Matrix3 Matrix3::lookAt(Point3 eye, Point3 target, Versor3 up) {
	Versor3 v = normalize(target - eye);
	Matrix3 m;
	m.z = v;
	m.x = normalize(cross(Versor3::up(), m.z));
	m.y = normalize(cross(m.z, m.x));
	return m;
}//TODO M-LookAt Done

// returns a rotation
Matrix3 Matrix3::toFrom(Versor3 to, Versor3 from) {
	// TODO M-ToFrom DONE
	return Matrix3::from(AxisAngle::toFrom(to,from));
}

Matrix3 Matrix3::toFrom(Vector3 to, Vector3 from) {
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
Matrix3 Matrix3::from(Quaternion m) {

	Matrix3 mat3;

	mat3.x.x = 1.0 - 2.0 * (m.y * m.y) - 2.0 * (m.z * m.z);
	mat3.x.y = 2.0 * (m.x * m.y) - 2.0 * (m.w * m.z);
	mat3.x.z = 2.0 * (m.x * m.z) + 2.0 * (m.w * m.y);

	mat3.y.x = 2.0 * (m.x * m.y) + 2.0 * (m.w * m.z);
	mat3.y.y = 1.0 - 2.0 * (m.x * m.x) - 2.0 * (m.z * m.z);
	mat3.y.z = 2.0 * (m.y * m.z) - 2.0 * (m.w * m.x);

	mat3.z.x = 2.0 * (m.x * m.z) - 2.0 * (m.w * m.y);
	mat3.z.y = 2.0 * (m.y * m.z) + 2.0 * (m.w * m.x);
	mat3.z.z = 1.0 - 2.0 * (m.x * m.x) - 2.0 * (m.y * m.y);

	
	return mat3;

}// TODO Q2M DONE
Matrix3 Matrix3::from(Euler e) {

	Matrix3 Rz = rotationZ(e.roll);
	Matrix3 Ry = rotationY(e.yaw);
	Matrix3 Rx = rotationX(e.pitch);
	return Rz * Rx * Ry;
}     // TODO E2M DONE

Matrix3 Matrix3::from(AxisAngle e) {
	Matrix3 m;
	Scalar m_cos = cos(e.angle);
	Scalar m_sin = sin(e.angle);
	Scalar offset = 1.0 - m_cos;

	m.x.x = m_cos + (e.axes.x * e.axes.x * offset);
	m.y.y = m_cos + (e.axes.y * e.axes.y * offset);
	m.z.z = m_cos + (e.axes.z * e.axes.z * offset);

	Scalar tmp_1 = e.axes.x * e.axes.y * offset;
	Scalar tmp_2 = e.axes.z * m_sin;
	m.x.y = tmp_1 + tmp_2;
	m.y.x = tmp_1 - tmp_2;

	tmp_1 = e.axes.x * e.axes.z * offset;
	tmp_2 = e.axes.y * m_sin;
	m.x.z = tmp_1 - tmp_2;
	m.z.x = tmp_1 + tmp_2;

	tmp_1 = e.axes.y * e.axes.z * offset;
	tmp_2 = e.axes.x * m_sin;
	m.y.z = tmp_1 + tmp_2;
	m.z.y = tmp_1 - tmp_2;

	return m;
} // TODO A2M done

Scalar Matrix3::det() const {
	return dot(cross(x, y), z.asVector());
}

// does this Matrix3 encode a rotation?
bool Matrix3::isRot() const {
	// TODO M-isR DONE
	bool isARotationMatrix = false;

	if (det() == 1.0 && cross(x, y).isEqual(z.asVector()) && cross(x, z).isEqual(y.asVector()) && cross(y, z).isEqual(x.asVector()))
		isARotationMatrix = true;

	return isARotationMatrix;
}

// return a rotation matrix around an axis
Matrix3 Matrix3::rotationX(Scalar angleDeg) {
	Matrix3 rotationX;
	Scalar rad = angleDeg * (M_PI/180.0);
	rotationX.x.x = 1.0;
	rotationX.x.y = 0.0;
	rotationX.x.z = 0.0;
	rotationX.y.x = 0.0;
	rotationX.y.y = cos(rad);
	rotationX.y.z = sin(rad);
	rotationX.z.x = 0.0;
	rotationX.z.y = -sin(rad);
	rotationX.z.z = cos(rad);

	return rotationX;
}   // TODO M-Rx DONE
Matrix3 Matrix3::rotationY(Scalar angleDeg) {
	Matrix3 rotationY; 
	Scalar rad = angleDeg * (M_PI / 180.0);
	rotationY.x.x = cos(rad);
	rotationY.x.y = 0.0;
	rotationY.x.z = -sin(rad);
	rotationY.y.x = 0.0;
	rotationY.y.y = 1.0;
	rotationY.y.z = 0.0;
	rotationY.z.x = sin(rad);
	rotationY.z.y = 0.0;
	rotationY.z.z = cos(rad);

	return rotationY;
}// TODO M-Ry DONE
Matrix3 Matrix3::rotationZ(Scalar angleDeg) {
	Matrix3 rotationZ;
	Scalar rad = angleDeg * (M_PI / 180.0);
	rotationZ.x.x = cos(rad);
	rotationZ.x.y = sin(rad);
	rotationZ.x.z = 0.0;
	rotationZ.y.x = -sin(rad);
	rotationZ.y.y = cos(rad);
	rotationZ.y.z = 0.0;
	rotationZ.z.x = 0.0;
	rotationZ.z.y = 0.0;
	rotationZ.z.z= 1.0;
	return rotationZ;
}  // TODO M-Rz DONE

void Matrix3::printf() const {
	std::cout << "Matrix3" << std::endl;
	x.printf();
	y.printf();
	z.printf();
} // TODO Print DONE
