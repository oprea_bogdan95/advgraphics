#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include "versor3.h"
/* Quaternion class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

class Matrix3;
class AxisAngle;
class Euler;

class Vector3;
class Point3;

typedef double Scalar;

class Quaternion {
public:

	/* fields */
	Scalar x, y, z, w;
	// TODO Q-Fields: which fields to store? (also add a constuctor taking these fields). DONE
	Quaternion(Scalar _x, Scalar _y, Scalar _z, Scalar _w);

	Quaternion(Scalar a, Scalar b, Scalar c);


	// TODO Q-Ide: this constructor construct the identity rotation DONE
	Quaternion();

	// TODO Q-FromPoint DONE
	// returns a quaternion encoding a point
	Quaternion(const Point3& p);

	Vector3 apply(Vector3  v)const;

	// Rotations can be applied to versors or vectors just as well
	Versor3 apply(Versor3 dir)const;

	Point3 apply(Point3 p)const;

	// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
	Versor3 operator() (Versor3 p);
	Point3  operator() (Point3  p);
	Vector3 operator() (Vector3 p);

	Versor3 axisX() const;  // TODO Q-Ax a
	Versor3 axisY() const;  // TODO Q-Ax b
	Versor3 axisZ() const;  // TODO Q-Ax c
	
	static Quaternion rotationX(Scalar angleDeg);   // TODO Q-Rx
	static Quaternion rotationY(Scalar angleDeg);   // TODO Q-Ry
	static Quaternion rotationZ(Scalar angleDeg);   // TODO Q-Rz

	Scalar squaredNormQ() const;

	void normalizeQ();
	// specific methods for quaternions...

	Quaternion operator * (Quaternion r)const;

	Quaternion inverse() const;

	void invert();

	Quaternion conjugated() const;
	// conjugate
	void conjugate();
	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static Quaternion lookAt(Point3 eye, Point3 target, Versor3 up = Versor3::up());

	// returns a rotation
	static Quaternion toFrom(Versor3 to, Versor3 from);

	static Quaternion toFrom(Vector3 to, Vector3 from);

	// conversions to this representation
	static Quaternion from(Matrix3 m);   // TODO M2Q
	static Quaternion from(Euler e);     // TODO E2Q

	static Quaternion from(AxisAngle e);

	// does this quaternion encode a rotation?
	bool isRot() const;
	// does this quaternion encode a poont?
	bool isPoint() const;

	void printf() const;
};


// interpolation or roations
inline Quaternion lerp(const Quaternion& a, const Quaternion& b, Scalar t) {
	// TODO Q-Lerp: how to interpolate quaternions DONE
	// hints: shortest path! Also, consdider them are 4D unit vectors.
	Quaternion r;
	Scalar t_ = 1 - t;
	r.x = t_ * a.x + t * b.x;
	r.y = t_ * a.y + t * b.y;
	r.z = t_ * a.z + t * b.z;
	r.w = t_ * a.w + t * b.w;
	r.normalizeQ();
	return r;
}



