#include <iostream>
#include "vector3.h"
#include "point3.h"


#include "matrix3.h"
#include "quaternion.h"
#include "axis_angle.h"
#include "euler.h"
/* AxisAngle class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

/* fields */
// TODO A-Field: which fields to store? (also add a constuctor taking these fields). DONE
AxisAngle::AxisAngle(Versor3 v, Scalar alpha) :axes(v), angle(alpha) {}

// TODO A-Ide: this constructor construct the identity rotation DONE
AxisAngle::AxisAngle() : axes(Versor3::fowrard()), angle(0) { }

// TODO A-FromPoint DONE
// returns a AxisAngle encoding a point
AxisAngle::AxisAngle(const Point3& p) : axes(normalize(p.asVector())), angle(0) {

}


//TODO A-up DONE
Vector3 AxisAngle::apply(Vector3  v) const {
	Quaternion q;
	return Vector3(q.from(*this).apply(v));
}

// Rotations can be applied to versors or vectors just as well
Versor3 AxisAngle::apply(Versor3 dir) const {
	return apply(dir.asVector()).asVersor();
}

Point3 AxisAngle::apply(Point3 p) const {
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
Versor3 AxisAngle::operator() (Versor3 p) { return apply(p); }
Point3  AxisAngle::operator() (Point3  p) { return apply(p); }
Vector3 AxisAngle::operator() (Vector3 p) { return apply(p); }

Versor3 AxisAngle::axisX() const {
	return normalize(angle * Versor3::right());
}  // TODO A-Ax a DONE 

Versor3 AxisAngle::axisY() const {
	return normalize(angle * Versor3::up());
}  // TODO A-Ax b  DONE 

Versor3 AxisAngle::axisZ() const {
	return normalize(angle * Versor3::fowrard());
}  // TODO A-Ax c  DONE 

AxisAngle AxisAngle::rotationX(Scalar angleDeg)
{
	Scalar rad = angleDeg * (M_PI / 180.0);
	return AxisAngle(Versor3::right(), rad);
}// TODO A-Rx Done

AxisAngle AxisAngle::rotationY(Scalar angleDeg)
{
	Scalar rad = angleDeg * (M_PI / 180.0);
	return AxisAngle(Versor3::up(), rad);
}// TODO A-Ry Done

AxisAngle AxisAngle::rotationZ(Scalar angleDeg)
{
	Scalar rad = angleDeg * (M_PI / 180.0);
	return AxisAngle(Versor3::fowrard(), rad);
}// TODO A-Rz Done

// conjugate
AxisAngle AxisAngle::operator * (AxisAngle r) const {

	return AxisAngle();
}

AxisAngle AxisAngle::inverse() const {
	// TODO A-Inv a DONE
	AxisAngle aa = *this;
	aa.invert();
	return aa;
}

void AxisAngle::invert() {
	// TODO A-Inv b DONE
	angle = -angle;
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
AxisAngle AxisAngle::lookAt(Point3 eye, Point3 target, Versor3 up) {
	// TODO A-LookAt DONE
	return AxisAngle::from(Matrix3::lookAt(eye, target, up));
}

// returns a rotation
AxisAngle AxisAngle::toFrom(Versor3 to, Versor3 from) {
	Versor3 rotationAxis = normalize(cross(from, to));
	Scalar angleCos = dot(from, to);
	Scalar angleRad = acos(angleCos);

	return AxisAngle(rotationAxis, angleRad);
}
AxisAngle AxisAngle::toFrom(Vector3 to, Vector3 from) {
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
AxisAngle AxisAngle::from(Matrix3 m) {

	const Vector3 res = Vector3(m.y.z - m.z.y, m.z.x - m.x.z, m.y.x - m.x.y);
	AxisAngle aa;
	aa.angle = acos((m.x.x + m.y.y + m.z.z - 1.0) / 2.0);
	aa.axes = normalize(res);
	return aa;
}   // TODO M2A DONE

AxisAngle AxisAngle::from(Euler e) {
	return from(Matrix3::from(e));
}     // TODO E2A DONE

AxisAngle AxisAngle::from(Quaternion q) {
	//Quaternion q(q);
	if (q.w > 1.0) {
		Scalar squaredNorm = (q.x * q.x) + (q.y * q.y) + (q.z * q.z) + (q.w * q.w);
		Scalar s;

		s = 1.0 / sqrt(squaredNorm);
		q.x *= s;
		q.y *= s;
		q.z *= s;
		q.w *= s;
	}

	AxisAngle aa;
	aa.angle = 2.0 * acos(q.w);
	Scalar temp = sqrt(1.0 - (q.w * q.w));
	Vector3 vec(q.x, q.y, q.z);
	aa.axes = normalize(vec / sin(aa.angle / 2.0));
	return aa;
}// TODO Q2A DONE

// does this AxisAngle encode a poont?
bool AxisAngle::isPoint() const {
	// TODO A-isP DONE
	bool isAPoint = false;
	isAPoint = (angle <= std::numeric_limits<Scalar>::epsilon());

	return isAPoint;
}

void AxisAngle::printf() const {
	axes.printf();
	std::cout << "angle: " << angle << std::endl;
} // TODO Print DONE
