#include <iostream>
#include "vector3.h"
#include "point3.h"
#include "versor3.h"

#include "matrix3.h"
#include "euler.h"
#include "quaternion.h"
#include "axis_angle.h"

/* Euler class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all the expected methods    */

	// TODO E-Ide: this constructor construct the identity rotation DONE
Euler::Euler() : pitch(0), yaw(0), roll(0) {}

Euler::Euler(Scalar _pitch, Scalar _yaw, Scalar _roll) : pitch(_pitch), yaw(_yaw), roll(_roll) {}

// TODO E-Constr DONE
// row major order!
Euler::Euler(Scalar m00, Scalar m01, Scalar m02,
	Scalar m10, Scalar m11, Scalar m12,
	Scalar m20, Scalar m21, Scalar m22)
{
}

Vector3 Euler::apply(Vector3  v) const {
	// TODO E-App: how to apply a rotation of this type?
	return Vector3();
}

// Rotations can be applied to versors or vectors just as well
Versor3 Euler::apply(Versor3 dir) const {
	return apply(dir.asVector()).asVersor();
}

Point3 Euler::apply(Point3 p) const {
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
Versor3 Euler::operator() (Versor3 p) { return apply(p); }
Point3  Euler::operator() (Point3  p) { return apply(p); }
Vector3 Euler::operator() (Vector3 p) { return apply(p); }

Versor3 Euler::axisX() const{
	return Versor3::up();
}  // TODO E-Ax a
Versor3 Euler::axisY() const{
	return Versor3::up();
}  // TODO E-Ax b
Versor3 Euler::axisZ() const{
	return Versor3::up();
}  // TODO E-Ax c

// conjugate
Euler Euler::operator * (Euler b) const {
	return Euler();
}

Euler Euler::inverse() const {
	// TODO E-Inv a
	return Euler();
}

void Euler::invert() const {
	// TODO E-Inv b
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
Euler Euler::lookAt(Point3 eye, Point3 target, Versor3 up ) {
	// TODO E-LookAt
	return Euler();
}

// returns a rotation
Euler Euler::toFrom(Versor3 to, Versor3 from) {
	// TODO E-ToFrom
	return Euler();
}

Euler Euler::toFrom(Vector3 to, Vector3 from) {
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
Euler Euler::from(Quaternion m) {
	return from(Matrix3::from(m));
}// TODO Q2E DONE

Euler Euler::from(Matrix3 m) { 
	Euler e;

	Scalar sp = -m.z.y;
	if (sp <= -1.0)
	{
		e.pitch = -M_PI_2;
	}
	else if (sp >= 1.0)
	{
		e.pitch = M_PI_2;
	}
	else
	{
		e.pitch = asin(sp);
	}

	//Check for the Gimbal Lock, give some tolerance
	Scalar tol = 1.0 - EPSILON;
	if (abs(sp) > tol)
	{
		//looking straight up or down
		e.roll = 0.0;
		e.yaw = atan2(-m.x.z, m.x.x);
	}
	else
	{
		e.yaw = atan2(m.z.x,m.z.z);
		e.roll = atan2(m.x.y,m.y.y);
	}
	e.roll = e.roll * 180 / M_PI;
	e.pitch = e.pitch * 180 / M_PI;
	e.yaw = e.yaw * 180 / M_PI;

	return e;
}     // TODO M2E DONE
Euler Euler::from(AxisAngle e) {
	return Euler();
} // TODO A2E

// does this Euler encode a rotation?
bool Euler::isRot() const {
	// TODO E-isR
	return false;
}

// return a rotation matrix around an axis

Euler Euler::rotationX(Scalar angleDeg){
	return Euler(angleDeg,0,0);
}  // TODO E-Rx DONE
Euler Euler::rotationY(Scalar angleDeg){
	return Euler(0, angleDeg, 0);
}  // TODO E-Ry DONE
Euler Euler::rotationZ(Scalar angleDeg){
	return Euler(0, 0, angleDeg);
}  // TODO E-Rz DONE

void Euler::printf() const {
	std::cout << "pitch: " << pitch << " yaw:" << yaw << " roll:" << roll << std::endl;
} // TODO Print DONE