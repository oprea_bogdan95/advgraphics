#include <iostream>
#include "transform.h"
#include "point3.h"
#include "versor3.h"
#include "matrix3.h"
#include "quaternion.h"
#include "axis_angle.h"
#include "euler.h"

using namespace std;
void check(bool condition, std::string descr) {
	std::cout << "Test " << descr << ": "
		<< ((condition) ? "PASSED" : "FAILED")
		<< "\n";

}

void unitTestInverse() {
	Transform t;
	t.translation = Vector3(2, 0, 0);
	t.scale = 3;

	Point3 p0, q, p1;

	q = t.apply(p0);

	Transform ti = t.inverse();
	p1 = ti.apply(q); // VOGLIO RIOTTENERE p

	check(p0.isEqual(p1), "INVERSE");
}

void unitTestCumlate() {
	Transform t1, t2;
	t1.translation = Vector3(2, 0, 0);
	t1.scale = 4;
	t1.rotation = Quaternion(90, 0, 0);

	t2.translation = Vector3(4, -3, 1);
	t2.scale = 0.4;
	t2.rotation = Quaternion(0, 0, 90);

	Point3 p(3, 1, 3);
	Point3 q0 = t2.apply(t1.apply(p));
	Transform product = t2 * t1;
	Point3 q1 = product.apply(p);

	check(q0.isEqual(q1), "CUMULATE");

}
void unitTestToFrom() {
	Vector3 from = Vector3(Versor3::up().asVector());
	Vector3 to = Vector3(Versor3::left().asVector());

	Matrix3 MatRot = Matrix3::toFrom(to, from);
	Vector3 Mat = MatRot.apply(from);
	
	Quaternion QuatRot = Quaternion::toFrom(to, from);
	Vector3 Quat = QuatRot.apply(from);
	AxisAngle AxisAngleRot = AxisAngle::toFrom(to, from);
	Vector3 axisAngle = AxisAngleRot.apply(from);

	
	check(axisAngle.isEqual(Mat), "FROM AxisAngle TO Mat");
	check(axisAngle.isEqual(Quat), "FROM AxisAngle TO Quat");
	check(Mat.isEqual(Quat), "FROM Mat TO Quat");
	

	std::cout << "Mat"; Mat.printf(); std::cout << std::endl;
	std::cout << "Quat"; Quat.printf(); cout << endl;
	std::cout << "AxisAngle"; axisAngle.printf(); cout << endl;

	
	check(Mat.isEqual(to), "Mat is equals to TO");
	check(Quat.isEqual(to), "Quat is equals to TO");
	check(axisAngle.isEqual(to), "AxisAngle is equals to TO");
}
void unitTestConversionBetweenRepresentations()
{
	Euler e(10, 0, 0);
	Euler resEuler = Euler::from(Matrix3::from(e));

	check(abs(e.pitch - resEuler.pitch) < EPSILON, "Euler Conversion X");
	check(abs(e.yaw - resEuler.yaw) < EPSILON, "Euler Conversion Y");
	check(abs(e.roll - resEuler.roll) < EPSILON, "Euler Conversion Z");

	Matrix3 m = Matrix3::rotationY(30);
	Matrix3 resMat = Matrix3::from(Euler::from(m));

	check(resMat.x.asVector().isEqual(m.x.asVector()), "Matrix3 Conversion X");
	check(resMat.y.asVector().isEqual(m.y.asVector()), "Matrix3 Conversion Y");
	check(resMat.z.asVector().isEqual(m.z.asVector()), "Matrix3 Conversion Z");

	AxisAngle axis = AxisAngle(Versor3::right(), 1.57082);
	AxisAngle as = AxisAngle::from(Quaternion::from(axis));
	check(axis.axes.asVector().isEqual(as.axes.asVector()), "aXIS ANGLE from quaternion AXIS");
	check(abs(axis.angle-as.angle) < EPSILON, "axis angle from quaternion angle");

	Quaternion quat(0.7071, 0, 0, 0.7071);
	Quaternion from = Quaternion::from(AxisAngle::from(quat));
	check(abs(quat.x - from.x) < EPSILON, "quaternion from axisAngle scalar x");
	check(abs(quat.y - from.y) < EPSILON, "quaternion from axisAngle scalar z");
	check(abs(quat.z - from.z) < EPSILON, "quaternion from axisAngle scalar y");
	check(abs(quat.w - from.w) < EPSILON, "quaternion from axisAnglew scalar w");

}

int main()
{
	unitTestInverse();
	std::cout << std::endl;
	unitTestCumlate();
	std::cout << std::endl;
	unitTestConversionBetweenRepresentations();
	std::cout << std::endl;
	unitTestToFrom();
	std::cout << std::endl;
	return 0;
}
