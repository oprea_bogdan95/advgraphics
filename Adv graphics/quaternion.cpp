#include <iostream>

#include "vector3.h"
#include "point3.h"

#include "matrix3.h"
#include "euler.h"
#include "axis_angle.h"
#include "quaternion.h"

/* Quaternion class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

	// TODO Q-Fields: which fields to store? (also add a constuctor taking these fields). DONE
Quaternion::Quaternion(Scalar _x, Scalar _y, Scalar _z, Scalar _w) : x(_x), y(_y), z(_z), w(_w) {

}

Quaternion::Quaternion(Scalar a, Scalar b, Scalar c) : x(a), y(b), z(c),w(0) {

}

// TODO Q-Ide: this constructor construct the identity rotation DONE
Quaternion::Quaternion() :x(0), y(0), z(0), w(1) {}

// TODO Q-FromPoint DONE
// returns a quaternion encoding a point
Quaternion::Quaternion(const Point3& p) : x(p.x), y(p.y), z(p.z), w(0) {
	// TODO DONE
}

Vector3 Quaternion::apply(Vector3  v) const {
	// TODO Q-App: how to apply a rotation of this type? DONE
	Quaternion quat = inverse();
	Quaternion point(v.asPoint());
	Quaternion result = (*this * point) * quat;
	Vector3 vec(result.x, result.y, result.z);
	return vec;
}

// Rotations can be applied to versors or vectors just as well
Versor3 Quaternion::apply(Versor3 dir) const {
	return apply(dir.asVector()).asVersor();
}

Point3 Quaternion::apply(Point3 p) const {
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
Versor3 Quaternion::operator() (Versor3 p) { return apply(p); }
Point3  Quaternion::operator() (Point3  p) { return apply(p); }
Vector3 Quaternion::operator() (Vector3 p) { return apply(p); }

Versor3 Quaternion::axisX() const {
	
	return inverse().apply(Versor3::right());
}  // TODO Q-Ax a DONE

Versor3 Quaternion::axisY() const {
	return inverse().apply(Versor3::up());
}   // TODO Q-Ax b DONE

Versor3 Quaternion::axisZ() const {
	 return inverse().apply(Versor3::fowrard());;
}   // TODO Q-Ax c DONE

Quaternion Quaternion::rotationX(Scalar angleDeg)
{	
	AxisAngle a;
	
	return Quaternion(from(a.rotationX(angleDeg)));
}// TODO Q-Rx done

Quaternion Quaternion::rotationY(Scalar angleDeg)
{
	AxisAngle a;

	return Quaternion(from(a.rotationY(angleDeg)));
}// TODO Q-Ry done

Quaternion Quaternion::rotationZ(Scalar angleDeg)
{
	AxisAngle a;

	return Quaternion(from(a.rotationZ(angleDeg)));
}// TODO Q-Rz done

Scalar Quaternion::squaredNormQ() const {

	return  (x * x) + (y * y) + (z * z) + (w * w);
}

void Quaternion::normalizeQ() {
	Scalar s;

	s = 1.0 / sqrt(squaredNormQ());
	x *= s;
	y *= s;
	z *= s;
	w *= s;
}
// specific methods for quaternions...

Quaternion Quaternion::operator * (Quaternion r) const {
	Quaternion quat(
		w * r.x + r.w * x + y * r.z - r.y * z,
		w * r.y + r.w * y + r.x * z - x * r.z,
		w * r.z + r.w * z + x * r.y - r.x * y,
		w * r.w - x * r.x - y * r.y - z * r.z);
	return quat;
}

Quaternion Quaternion::inverse() const {
	//TODO Q-Inv a  DONE
	Quaternion quat = *this;
	quat.invert();
	return quat;
}

void Quaternion::invert() {
	//TODO Q - Inv b DONE
	Quaternion quat;
	quat = *this;
	x = -quat.x / quat.squaredNormQ();
	y = -quat.y / quat.squaredNormQ();
	z = -quat.z / quat.squaredNormQ();
	w = quat.w / quat.squaredNormQ();
}

Quaternion Quaternion::conjugated() const {
	// TODO Q-Conj a DONE
	Quaternion q = *this;
	q.conjugate();
	return q;
}
// conjugate
void Quaternion::conjugate() {
	// TODO Q-Conj b DONE
	x = -x;
	y = -y;
	z = -z;
	w = w;
}
// returns a rotation to look toward target, if you are in eye, and the up-vector is up
Quaternion Quaternion::lookAt(Point3 eye, Point3 target, Versor3 up) {
	//TODO Q-lookAt DONE
	
	return from(AxisAngle::from(Matrix3::lookAt(eye, target, up)));
}

// returns a rotation
Quaternion Quaternion::toFrom(Versor3 to, Versor3 from) {
	// TODO Q-ToFrom DONE
	return Quaternion::from(AxisAngle::toFrom(to,from));
	
}

Quaternion Quaternion::toFrom(Vector3 to, Vector3 from) {
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
Quaternion Quaternion::from(Matrix3 m) {

	return from(AxisAngle::from(m));
}   // TODO M2Q DONE

Quaternion Quaternion::from(Euler e) {
	return Quaternion();
}    // TODO E2Q

Quaternion Quaternion::from(AxisAngle e) {
	Scalar s = sin(e.angle / 2.0);
	Vector3 vec = e.axes * s;
	return Quaternion(vec.x, vec.y, vec.z, cos(e.angle / 2.0));
} // TODO A2Q DONE

// does this quaternion encode a rotation?
bool Quaternion::isRot() const {
	// TODO Q-isR DONE
	bool isARot = false;
	Scalar s = squaredNormQ();
	isARot = (s - 1 <= std::numeric_limits<Scalar>::epsilon() * std::numeric_limits<Scalar>::epsilon());
	return false;
}

// does this quaternion encode a poont?
bool Quaternion::isPoint() const {
	//TODO Q - isP DONE
	bool isAPoint = false;
	isAPoint = w <= std::numeric_limits<Scalar>::epsilon();
	return isAPoint;
}

void Quaternion::printf() const {
	std::cout << "(x: " << x << " y: " << y << " z: " << z << " w: " << w << ")" << std::endl;
} // TODO Print Done
